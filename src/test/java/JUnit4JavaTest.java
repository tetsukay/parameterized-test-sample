import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class JUnit4JavaTest {

    private final String str;
    private final int length;

    public JUnit4JavaTest(String str, int length) {
        this.str = str;
        this.length = length;
    }

    @Parameterized.Parameters(name = "\"{0}\" length is {1}.")
    public static Iterable<Object> data() {
        return Arrays.asList(
                new Object[][] {
                        { "1", 1 },
                        { "0000", 4 },
                        { "1234", 5 },
                }
        );
    }

    @Test
    public void checkStringLength() {
        Assert.assertEquals(str.length(), length);
    }

}