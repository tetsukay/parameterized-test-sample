import org.junit.Assert;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

@RunWith(Theories.class)
public class JUnit4TheoriesTest {

    static class Pair<F, S> {
        final F first;
        final S second;

        Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }
    }

    @DataPoints
    public static List<Pair<String, Integer>> data = Arrays.asList(
            new Pair<>("1", 1),
            new Pair<>("0000", 4),
            new Pair<>("1234", 5)
    );

    @Theory
    public void checkStringLength(Pair<String, Integer> data) {
        Assert.assertEquals(data.first.length(), data.second.intValue());
    }
}
