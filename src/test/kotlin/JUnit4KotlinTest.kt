import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class JUnit4KotlinTest(private val str: String, private val length: Int) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "\"{0}\" length is {1}.")
        fun data(): Collection<Array<Any>> {
            return listOf(
                    arrayOf("1", 1),
                    arrayOf("0000", 4),
                    arrayOf("1234", 5)
            )
        }
    }

    @Test
    fun checkStringLength() {
        assertEquals(str.length, length)
    }
}