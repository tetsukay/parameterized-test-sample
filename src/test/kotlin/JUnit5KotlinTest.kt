import org.junit.Assert
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class JUnit5KotlinTest {
    companion object {

        class TestArgumentsProvider : ArgumentsProvider {
            override fun provideArguments(context: ExtensionContext?): Stream<out Arguments>
                    = Stream.of(
                    Pair("1", 1),
                    Pair("0000", 4),
                    Pair("1234", 5)
            ).map { Arguments.of(it) }
        }

        @JvmStatic
        fun data(): Stream<Arguments> = Stream.of(
                Arguments.of("1", 1),
                Arguments.of("0000", 4),
                Arguments.of("1234", 5)
        )
    }

    @ParameterizedTest(name = "\"{0}\" length is {1}.")
    @MethodSource("data")
    fun checkStringLength1(str: String, length: Int) {
        Assert.assertEquals(str.length, length)
    }

    @ParameterizedTest
    @ArgumentsSource(TestArgumentsProvider::class)
    fun checkStringLength2(pair: Pair<String, Int>) {
        Assert.assertEquals(pair.first.length, pair.second)
    }


}
