import org.junit.Assert
import org.junit.jupiter.api.Test

class Test {
    @Test
    fun checkStringLength() {
        Assert.assertEquals(1, 1)
        Assert.assertEquals(1, 1)
        Assert.assertEquals(1, 2)
        Assert.assertEquals(1, 1)
        Assert.assertEquals(1, 1)
        Assert.assertEquals(1, 2)
    }
}
